# Slides

We're being pretty particular with the language used here. Here are some definitions:

SSH words: 

- Client (`ssh`, starts connection)
- Server (`sshd`, listening)

Tunnel words: 

- Bind (interface/port)
- Target (address/port)

## `-L` - Client listener, New connection via Server

Example syntax:

```
-L bind_interface:bind_port:target_address:target_port
```

The `-L` creates a new listening socket at the specificied `bind_interface` and
`bind_port` on the __CLIENT__ side that will shuttle packets received at that
__CLIENT__ side socket listener through the secure ssh connection all the way
to the ssh __SERVER__ where the __SERVER__ then iniitiates a new connection to
the `target_host` and `target_port`

> __Jargon:__ this is often called a Forward Tunnel

### Use Case

I would like to connect to an RDP server (`192.168.34.35`) that is only
accessible on a closed network that an ssh server (`example.com`) that I 
have access to is also on. I will create a forward tunnel that listens on 
the clients interface (`127.0.0.1`) at port `3389` which will transit the 
secure connection. Once on the other side the ssh server will create a new 
connection to the target RDP server on port `3389`. 

```
ssh example.com -L 127.0.0.1:3389:192.168.34.35:3389
```

> __Jargon__: This example tunnel is often called a Straight Through Tunnel
> because the client listening port is the same as the target port
  

## `-R` - Server listener, New connection via Client

Example syntax:

```
-R bind_interface:bind_port:target_address:target_port
```

The `-R` creates a new listening socket at the specificied `bind_interface` and
`bind_port` on the __SERVER__ side that will shuttle packets received at that
__SERVER__ side socket listener through the secure ssh connection all the way
to the ssh __CLIENT__ where the __CLIENT__ then iniitiates a new connection to
the `target_host` and `target_port`

> __Jargon:__ This is often called a Reverse Tunnel

### Use Case

There is an existing web service that is running on example.com.  It is
configured to connect to its mysql instance at `127.0.0.1` and port `3306`. I
want to stop the mysql service running on example.com and run a test against a
different set of data that is only on my ssh client machine. This database is
3TB so I definitely don't want to send it up to the server just to do this
test.

After stopping the existing mysql service I connect into `example.com` with a
reverse tunnel that listens on the servers interface (`127.0.0.1`) at port
`3306` which will transit the secure connection. Once on the other side the ssh
client will create a new connection to the target mysql server on port `5555`
because this is where I have this specific mysql instance listening. 

```
ssh example.com -R 127.0.0.1:3306:127.0.0.1:5555
```

## `-D` - Client socks listener, Multiple connections via Server


Example syntax:

```
-D bind_interface:bind_port
```

The `-D` creates a new listening socket at the specificied `bind_interface` and
`bind_port` on the __CLIENT__ side which is a __SOCKS PROXY__ listener.
__APPLICATION TRAFFIC__ sent to this the __CLIENT__ side __SOCKS PROXY__
listener will transit the secure ssh connection all the way to the ssh
__SERVER__ where the __SERVER__ then iniitiates a new connection to the
__APPLICATION SPECIFIED__ targets

> __Jargon:__ This is often called a Dynamic Proxy

### Use Case

I want to browse an internal website that requires multiple connections 
to multiple domains and IP addresses on many different ports. The ssh 
server (`example.com`) has the appropriate network connectivity to initiate
all of these connections. I will create a dynamic tunnel proxy at port
`9050`. I will then configure my web browser to use that tunnel as a socks
proxy. After this is setup all connections from firefox will be sent to 
the socks proxy listener, transit the secure connection, and egress to the
correct target hosts and ports on the server's network.

```
ssh example.com -D 127.0.0.1:8080
```

## Quiz

If a `target_host` is specified as a DNS name, where is that name resolved?

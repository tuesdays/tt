## Tunneling Training

#### Install

```
# ubuntu 20.04 [aarch64 or x86_64]
sudo apt install docker.io docker-compose
cd range
export FQDN=yyyyyyyyyyyyyyyyyyyyyyyy.com
sudo -E docker-compose build
sudo -E docker-compose up -d
```

#### Overview: 
Tunneling and Unix enumeration with hands on labs focusing on 
understanding environmental clues to solve spooky puzzles and 
practicing key redirection spells that every ghoul should have 
in their grimoire. Too spooky for some, Intermediate level training.
Labs will be Instructor-led and walk-throughs of solutions will 
also be provided throughout the day and recorded.


Installing on a few machines:
```
ansible all -f 30 -i host.yml -m shell -a 'export FQDN={{ inventory_hostname }}; sudo apt update; sudo apt install -y docker.io docker-compose; git clone https://gitlab.com/tuesdays/tbd.git || true; cd tbd/range; sudo -E docker-compose build; sudo -E docker-compose up -d'
```
